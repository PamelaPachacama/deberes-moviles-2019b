class Person private constructor() {
    var name: String? = null
        private set
    var surname: String? = null
       private set
    class Builder internal constructor() {
        private var personToBuild: Person? = null
        init {
            personToBuild = Person()
        }
        internal fun build(): Person? {
            val builtPerson = personToBuild
            personToBuild = Person()
            return builtPerson
        }
        fun setName(name: String): Builder {
            this.personToBuild!!.name = name
            return this
        }
        fun setSurname(surname: String): Builder {
            this.personToBuild!!.surname = surname
            return this
        }
    }
}