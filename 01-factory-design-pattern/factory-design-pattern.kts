interface Book{
    fun getInfo()
    fun order()
    fun rate()
}
enum class Genre {
    FANTASY, LITERATURE
}
class BookFactory{
    companion object{
        fun createBook(genre:Genre): Book = when (genre){
            Genre.FANTASY -> object: Book {
                private val title = "Harry Potter"
                override fun getInfo() = println("$title")
                override fun order() = println("Order $title")
                override fun rate() = println("Rate for $title")
            }
            Genre.LITERATURE -> object: Book {
                private val title = "ROMEO AND JULIETA"
                override fun getInfo() = println("$title")
                override fun order() = println("Order $title")
                override fun rate() = println("Rate for $title")
            }
        }
    }
}
fun main(args: Array<String>) {
    val genre = "FANTASY"
    val book = BookFactory.createBook(when(genre){
        "FANTASY" -> Genre.FANTASY
        "LITERATURE" -> Genre.LITERATURE
        else -> throw IllegalArgumentException("There is no such genre")
    })
    println("Getting book info: ")
    book.getInfo()
    println("Order book: ")
    book.order()
    println("Rate book: ")
    book.rate()
}